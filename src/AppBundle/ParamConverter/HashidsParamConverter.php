<?php


namespace AppBundle\ParamConverter;


use Hashids\Hashids;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class HashidsParamConverter implements ParamConverterInterface
{
    const HASHIDS_IDENTIFIER = 'hashid';

    /**
     * @var Hashids
     */
    private $hashids;

    /**
     * HashidsParamConverter constructor.
     *
     * @param Hashids $hashids
     */
    public function __construct(Hashids $hashids)
    {
        $this->hashids = $hashids;
    }

    /**
     * Replaces the hashids with ids
     *
     * @param Request        $request       The request
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return bool True if the object has been successfully set, else false
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $names = array_keys($request->attributes->all());

        $setSomething = false;

        foreach ($names as $name) {
            $lowerName = strtolower($name);
            if (strpos($lowerName, self::HASHIDS_IDENTIFIER) !== false) {
                $decoded = $this->hashids->decode($request->attributes->get($name));

                if (empty($decoded)) {
                    throw new \LogicException('Unable to decode hashid');
                }

                $request->attributes->set($name, $decoded[0]);

                $setSomething = true;
            }
        }

        return $setSomething;
    }


    /**
     * Checks if the object is supported.
     *
     * @param ParamConverter $configuration Should be an instance of ParamConverter
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        return
            strpos($configuration->getName(), self::HASHIDS_IDENTIFIER) !== false ||
            strpos($configuration->getName(), ucfirst(self::HASHIDS_IDENTIFIER)) !== false;
    }
}
