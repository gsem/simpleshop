<?php


namespace AppBundle\Services;


use AppBundle\Entity\ItemOrder;

class AmountCalculator
{
    /**
     * @param ItemOrder[] $orders
     *
     * @return float|int
     */
    public function calculateOrderTotal(array $orders)
    {
        $total = 0;

        foreach ($orders as $order) {
            $total += $order->getItem()->getPrice();
        }

        return $total;
    }
}
