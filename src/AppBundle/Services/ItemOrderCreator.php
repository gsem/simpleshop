<?php


namespace AppBundle\Services;


use AppBundle\Entity\Item;
use AppBundle\Entity\ItemOrder;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ItemOrderCreator
 *
 * @package AppBundle\Services
 */
class ItemOrderCreator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ItemOrderCreator constructor.
     *
     * @param EntityManagerInterface $em
     * @param ValidatorInterface     $validator
     */
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @param User $user
     * @param Item $item
     *
     * @param bool $flush
     *
     * @return ItemOrder
     */
    public function createItemOrder(User $user, Item $item, $flush = true)
    {
        $itemOrder = new ItemOrder();

        $itemOrder->setMarking('cart');
        $itemOrder->setItem($item);
        $itemOrder->setUser($user);


        $violations = $this->validator->validate($itemOrder);

        if (count($violations) > 0) {
            foreach ($violations as $violation) {
                throw new ValidatorException($violation->getMessage());
            }
        }

        $this->em->persist($itemOrder);

        if ($flush) {
            $this->em->flush();
        }

        return $itemOrder;
    }
}
