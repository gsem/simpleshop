<?php


namespace AppBundle\Validator;


use AppBundle\Entity\ItemOrder;
use AppBundle\Repository\ItemOrderRepository;
use AppBundle\Validator\Constraints\UniqueOrderConstraint;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ValidatorException;

class UniqueOrderConstraintValidator extends ConstraintValidator
{
    /**
     * @var ItemOrderRepository
     */
    private $itemOrderRepository;

    /**
     * UniqueOrderConstraintValidator constructor.
     *
     * @param ItemOrderRepository $itemOrderRepository
     */
    public function __construct(ItemOrderRepository $itemOrderRepository)
    {
        $this->itemOrderRepository = $itemOrderRepository;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param ItemOrder  $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (false === $constraint instanceof UniqueOrderConstraint) {
            throw new ValidatorException("Constraint unsupported");
        }

        $conflictingMarkings = [
            ItemOrder::WF_MARKING_REQUEST_PENDING,
            ItemOrder::WF_MARKING_CART,
        ];

        $results = $this->itemOrderRepository->getByUserItemMarkings(
            $value->getUser(),
            $value->getItem(),
            $conflictingMarkings
        );

        if (!empty($results)) {
            $this->context->buildViolation("Order is already in cart or pending.")->addViolation();
        }
    }
}
