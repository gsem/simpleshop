<?php


namespace AppBundle\Validator;


use AppBundle\Entity\ItemOrder;
use AppBundle\Validator\Constraints\OwnItemConstraint;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ValidatorException;

class OwnItemConstraintValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param ItemOrder  $value      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (false === $constraint instanceof OwnItemConstraint) {
            throw new ValidatorException("Constraint unsupported");
        }

        if ($value->getItem()->getSeller()->getId() == $value->getUser()->getId()) {
            $this->context->buildViolation("You can't buy your own item.")->addViolation();
        }
    }
}
