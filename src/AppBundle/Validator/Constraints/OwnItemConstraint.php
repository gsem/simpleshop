<?php


namespace AppBundle\Validator\Constraints;


use AppBundle\Validator\OwnItemConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class OwnItemConstraint extends Constraint
{
    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return mixed
     */
    public function validatedBy()
    {
        return OwnItemConstraintValidator::class;
    }
}
