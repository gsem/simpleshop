<?php


namespace AppBundle\Validator\Constraints;


use AppBundle\Validator\UniqueOrderConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueOrderConstraint extends Constraint
{
    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return mixed
     */
    public function validatedBy()
    {
        return UniqueOrderConstraintValidator::class;
    }
}
