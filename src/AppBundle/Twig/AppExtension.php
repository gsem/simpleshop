<?php


namespace AppBundle\Twig;


class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('price', [$this, 'priceFilter']),
            new \Twig_SimpleFilter('paintWorkflow', [$this, 'workflowPainterFilter']),
        ];
    }

    public function priceFilter($number, $decimals = 2, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '€'.$price;

        return $price;
    }

    public function workflowPainterFilter($state)
    {
        $map = [
            'Accepted' => 'label-success',
            'Declined' => 'label-danger',
            'Pending' => 'label-warning',
        ];

        if (array_key_exists($state, $map)) {
            $state = "<span class=\"label {$map[$state]}\">{$state}</span>";
        }

        return $state;
    }

    public function getName()
    {
        return 'app_extension';
    }
}
