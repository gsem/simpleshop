<?php


namespace AppBundle\Form\Type;


use AppBundle\Entity\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'label.item.title',
                ]
            )
            ->add(
                'description',
                TextType::class,
                [
                    'label' => 'label.item.description',
                ]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                    'label' => 'label.item.price',
                ]
            )
            ->add(
                'imageFile',
                FileType::class,
                [
                    'label' => 'label.item.image',
                ]
            )
            ->add(
                'category',
                ChoiceType::class,
                [
                    'choices' => Item::getAvailableCategories(),
                    'multiple' => false,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Item::class,
            ]
        );
    }
}
