<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * ItemOrder
 *
 * @ORM\Table(name="item_order")
 *
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemOrderRepository")
 * @AppAssert\UniqueOrderConstraint()
 * @AppAssert\OwnItemConstraint()
 */
class ItemOrder
{
    const WF_MARKING_CART = 'cart';
    const WF_MARKING_CART_REMOVED = 'cart_removed';
    const WF_MARKING_REQUEST_PENDING = 'request_pending';
    const WF_MARKING_REQUEST_DECLINED = 'request_declined';
    const WF_MARKING_REQUEST_ACCEPTED = 'request_accepted';

    const TRANSITION_ACCEPTING = 'accepting';
    const TRANSITION_DECLINING = 'declining';
    const TRANSITION_REQUESTING = 'requesting';
    const TRANSITION_REMOVING = 'removing';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="marking", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $marking;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Item")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    protected $item;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    protected $user;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     */
    protected $updatedAt;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     */
    protected $createdAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        if ($this->getUpdatedAt() === null) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function resetUpdatedAt()
    {
        // update the modified time
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marking
     *
     * @param string $marking
     *
     * @return ItemOrder
     */
    public function setMarking($marking)
    {
        $this->marking = $marking;

        return $this;
    }

    /**
     * Get marking
     *
     * @return string
     */
    public function getMarking()
    {
        return $this->marking;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ItemOrder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ItemOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set item
     *
     * @param Item $item
     *
     * @return ItemOrder
     */
    public function setItem(Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return ItemOrder
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
