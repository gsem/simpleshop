<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as FOSUser;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends FOSUser
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var Item[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Item",
     *      mappedBy="seller"
     * )
     */
    protected $itemsForSale;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add itemsForSale
     *
     * @param Item $itemsForSale
     *
     * @return User
     */
    public function addItemsForSale(Item $itemsForSale)
    {
        $this->itemsForSale[] = $itemsForSale;

        return $this;
    }

    /**
     * Remove itemsForSale
     *
     * @param Item $itemsForSale
     */
    public function removeItemsForSale(Item $itemsForSale)
    {
        $this->itemsForSale->removeElement($itemsForSale);
    }

    /**
     * Get itemsForSale
     *
     * @return ArrayCollection
     */
    public function getItemsForSale()
    {
        return $this->itemsForSale;
    }
}
