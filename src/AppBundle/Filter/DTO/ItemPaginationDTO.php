<?php


namespace AppBundle\Filter\DTO;


use DataDog\PagerBundle\Pagination;

class ItemPaginationDTO
{
    /**
     * @var Pagination
     */
    protected $items;

    /**
     * @var array
     */
    protected $availableCategories;

    /**
     * ItemPaginationDTO constructor.
     *
     * @param Pagination $items
     * @param array      $availableCategories
     */
    public function __construct(Pagination $items, array $availableCategories)
    {
        $this->items = $items;
        $this->availableCategories = $availableCategories;
    }

    /**
     * @return Pagination
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function getAvailableCategories()
    {
        return $this->availableCategories;
    }
}
