<?php


namespace AppBundle\Filter;


use AppBundle\Entity\Item;
use AppBundle\Filter\DTO\ItemPaginationDTO;
use DataDog\PagerBundle\Pagination;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

class ItemPaginationBuilder
{
    /**
     * @var ItemFilter
     */
    private $itemFilter;

    /**
     * ItemPaginationBuilder constructor.
     *
     * @param ItemFilter $itemFilter
     */
    public function __construct(ItemFilter $itemFilter)
    {
        $this->itemFilter = $itemFilter;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param Request      $request
     *
     * @return Pagination
     */
    public function createPagination(QueryBuilder $queryBuilder, Request $request)
    {
        $items = new Pagination(
            $queryBuilder, $request, [
                'applyFilter' => [$this->itemFilter, 'filter'],
            ]
        );

        $availableCategories = array_merge(
            [
                'No filter' => Pagination::$filterAny,
            ],
            Item::getAvailableCategories()
        );

        return new ItemPaginationDTO($items, array_flip($availableCategories));
    }
}
