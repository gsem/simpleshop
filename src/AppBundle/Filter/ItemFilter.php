<?php


namespace AppBundle\Filter;

use Doctrine\ORM\QueryBuilder;

class ItemFilter
{
    /**
     * Our filter handler function, which allows us to
     * modify the query builder specifically for our filter option
     *
     * @param QueryBuilder $qb
     * @param              $key
     * @param              $val
     *
     * @throws \Exception
     */
    public function filter(QueryBuilder $qb, $key, $val)
    {
        switch ($key) {
            case 'category':
                if ($val) {
                    $qb
                        ->andWhere($qb->expr()->like('i.category', ':category'))
                        ->setParameter('category', "%$val%");
                }
                break;
            default:
                throw new \Exception("filter not allowed");
        }
    }
}
