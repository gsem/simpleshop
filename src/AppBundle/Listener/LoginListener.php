<?php


namespace AppBundle\Listener;


use AppBundle\Entity\Item;
use AppBundle\Services\ItemOrderCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Hashids\Hashids;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class LoginListener
 *
 * @package AppBundle\Listener
 */
class LoginListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var ItemOrderCreator
     */
    private $itemOrderCreator;
    /**
     * @var Hashids
     */
    private $hashids;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * LoginListener constructor.
     *
     * @param EntityManagerInterface $em
     * @param ItemOrderCreator       $itemOrderCreator
     * @param Hashids                $hashids
     * @param SessionInterface       $session
     */
    public function __construct(
        EntityManagerInterface $em,
        ItemOrderCreator $itemOrderCreator,
        Hashids $hashids,
        SessionInterface $session
    ) {
        $this->em = $em;
        $this->itemOrderCreator = $itemOrderCreator;
        $this->hashids = $hashids;
        $this->session = $session;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onLogin(InteractiveLoginEvent $event)
    {
        $cookies = $event->getRequest()->cookies;

        if ($cookies->has('cart')) {
            $itemRepo = $this->em->getRepository(Item::class);
            $ids = json_decode($cookies->get('cart'));
            foreach ($ids as $id) {
                $id = $this->hashids->decode($id)[0];
                if ($item = $itemRepo->find($id)) {
                    try {
                        $this->itemOrderCreator->createItemOrder(
                            $event->getAuthenticationToken()->getUser(),
                            $item,
                            false
                        );
                    } catch (Exception $e) {
                        $this->session->getFlashBag()->add('warning', $e->getMessage());
                    }
                }
            }

            $this->em->flush();

            $event->getRequest()->cookies->set('cart', null);
        }
    }
}
