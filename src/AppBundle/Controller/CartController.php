<?php


namespace AppBundle\Controller;


use AppBundle\Entity\ItemOrder;
use AppBundle\Repository\ItemOrderRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CartController extends Controller
{
    /**
     * @Route("/cart", name="view-cart")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login or create an account first!');
            throw $this->createAccessDeniedException();
        }

        /** @var ItemOrderRepository $itemOrderRepository */
        $itemOrderRepository = $this->getDoctrine()->getManager()->getRepository(ItemOrder::class);

        /** @var ItemOrder[] $orders */
        $orders = $itemOrderRepository->getCartItems($this->getUser());

        return $this->render(
            '@App/Cart/cart.html.twig',
            [
                'orders' => $orders,
                'totalAmount' => $this->get('app.services.amount_calculator')->calculateOrderTotal($orders),
                'orderCount' => count($orders),
            ]
        );
    }

    /**
     * @Route("/cart", name="submit-cart")
     * @Method("POST")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function submitAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login first!');
            throw $this->createAccessDeniedException();
        }

        /** @var ItemOrderRepository $itemOrderRepository */
        $itemOrderRepository = $this->getDoctrine()->getManager()->getRepository(ItemOrder::class);

        /** @var ItemOrder[] $orders */
        $orders = $itemOrderRepository->getCartItems($this->getUser());

        foreach ($orders as $order) {
            $this->get('workflow.order')->apply($order, ItemOrder::TRANSITION_REQUESTING);
        }

        $this->get('doctrine')->getManager()->flush();

        $this->addFlash('success', 'Buy requests were sent to the sellers. You can see them in Your Requests');

        return $this->redirectToRoute('view-cart');
    }
}
