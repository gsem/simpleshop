<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use AppBundle\Filter\DTO\ItemPaginationDTO;
use AppBundle\Form\Type\ItemType;
use AppBundle\Repository\ItemRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class ItemController extends Controller
{
    /**
     * @Route("/items", name="sell-item")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login first!');
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(ItemType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Item $item */
            $item = $form->getData();

            $item->setSeller($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();

            $this->addFlash('success', 'Item added!');

            return $this->redirectToRoute('my-items');
        }

        return $this->render(
            '@App/Item/sell_item.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/my-items", name="my-items")
     * @Method({"GET"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myItemsAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login first!');
            throw $this->createAccessDeniedException();
        }

        /** @var ItemRepository $itemRepository */
        $itemRepository = $this->getDoctrine()->getManager()->getRepository(Item::class);

        /** @var ItemPaginationDTO $itemPagination */
        $itemPagination = $this->get('app.filter.pagination_builder')->createPagination(
            $itemRepository->getItemsByUserQb($this->getUser()),
            $request
        );

        return $this->render(
            '@App/Item/my_items.html.twig',
            [
                'items' => $itemPagination->getItems(),
                'availableCategories' => $itemPagination->getAvailableCategories(),
            ]
        );
    }

    /**
     * @Route("/",name="homepage")
     * @Route("/items-for-sale",name="items-for-sale")
     * @Method({"GET"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var ItemRepository $itemRepository */
        $itemRepository = $this->getDoctrine()->getManager()->getRepository(Item::class);

        /** @var ItemPaginationDTO $itemPagination */
        $itemPagination = $this->get('app.filter.pagination_builder')->createPagination(
            $itemRepository->getAllItemsQb(),
            $request
        );

        $response = $this->render(
            '@App/Item/index.html.twig',
            [
                'items' => $itemPagination->getItems(),
                'availableCategories' => $itemPagination->getAvailableCategories(),
            ]
        );

        if ($this->getUser()) {
            $response->headers->clearCookie('cart');
        }

        return $response;
    }

    /**
     * @Route("/items/{hashid}",requirements={"hashid"="[A-Za-z0-9_-]+"},name="view-item")
     * @Method({"GET"})
     * @ParamConverter("hashid")
     *
     * @param $hashid
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ResourceNotFoundException
     */
    public function viewAction($hashid)
    {
        if (empty($hashid)) {
            throw new ResourceNotFoundException();
        }

        /** @var ItemRepository $itemRepository */
        $itemRepository = $this->getDoctrine()->getManager()->getRepository(Item::class);

        $item = $itemRepository->find($hashid);

        if (empty($item)) {
            throw new ResourceNotFoundException();
        }

        return $this->render(
            '@App/Item/view.html.twig',
            [
                'item' => $item,
            ]
        );
    }

}
