<?php


namespace AppBundle\Controller;


use AppBundle\Entity\ItemOrder;
use AppBundle\Repository\ItemOrderRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/navbar", name="navbar-esi")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function navigationBarAction()
    {
        $cartItems = [];
        $pendingOrders = [];

        if ($user = $this->getUser()) {
            /** @var ItemOrderRepository $itemOrderRepo */
            $itemOrderRepo = $this->getDoctrine()->getManager()->getRepository(ItemOrder::class);

            $cartItems = $itemOrderRepo->getCartItems($user);
            $pendingOrders = $itemOrderRepo->getPendingUserOrders($user);
        }

        return $this->render(
            ':default:navbar.html.twig',
            [
                'cartCount' => count($cartItems),
                'pendingOrders' => count($pendingOrders),
            ]
        );
    }
}
