<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Item;
use AppBundle\Entity\ItemOrder;
use AppBundle\Repository\ItemOrderRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Validator\Exception\ValidatorException;

class ItemOrderController extends Controller
{
    /**
     * @Route("/order/item/{hashid}", name="add-to-basket")
     * @Method("POST")
     * @param Item $item
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Item $item)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login first!');
            throw $this->createAccessDeniedException();
        }

        try {
            $this->get('app.services.item_order_creator')->createItemOrder($this->getUser(), $item);
        } catch (ValidatorException $e) {
            $this->addFlash('danger', $e->getMessage());

            return $this->redirectToRoute('homepage');
        }

        $this->addFlash('success', 'Item added to basket!');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/orders", name="view-orders")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login first!');
            throw $this->createAccessDeniedException();
        }

        /** @var ItemOrderRepository $itemOrderRepository */
        $itemOrderRepository = $this->getDoctrine()->getManager()->getRepository(ItemOrder::class);

        /** @var ItemOrder[] $orders */
        $outgoingOrders = $itemOrderRepository->getOutgoingUserOrders($this->getUser());
        $incomingOrders = $itemOrderRepository->getIncomingUserOrders($this->getUser());
        $pendingOrders = $itemOrderRepository->getPendingUserOrders($this->getUser());

        return $this->render(
            '@App/ItemOrder/index.html.twig',
            [
                'outgoingOrders' => $outgoingOrders,
                'incomingOrders' => $incomingOrders,
                'pendingOrders' => $pendingOrders,
            ]
        );
    }

    /**
     * @Route("/orders/{hashid}/apply-transition", name="order_apply_transition")
     * @Method("POST")
     *
     * @param Request   $request
     * @param ItemOrder $order
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function applyTransitionAction(Request $request, ItemOrder $order)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->addFlash('warning', 'You should login first!');
            throw $this->createAccessDeniedException();
        }

        $transition = $request->query->get('transition');

        if (empty($transition)) {
            throw new InvalidParameterException('Transition should be provided');
        }

        $this->get('workflow.order')->apply($order, $transition);
        $this->get('doctrine')->getManager()->flush();

        return $this->redirect($request->headers->get('referer'));
    }
}
