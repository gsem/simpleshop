/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

(function ($) {
    var getCartCount = function () {
        var cart = [];
        if (Cookies.get("cart")) {
            cart = JSON.parse(Cookies.get("cart"));
        }

        return cart.length;
    };

    $(document).ready(function () {
        var addCartButton = $(".add-cart-button");
        var navCartBadge = $(".cart-badge");

        hljs.initHighlightingOnLoad();
        if (navCartBadge) {
            navCartBadge.text(getCartCount());
        }

        addCartButton.on('click', function () {
            var cart = [];
            if (Cookies.get("cart")) {
                cart = JSON.parse(Cookies.get("cart"));
            }

            var itemId = $(this).data('itemId');

            if ($.inArray(itemId, cart) === -1) {
                cart.push(itemId);
                Cookies.set('cart', JSON.stringify(cart));
            } else {
                alert('Item is already in a cart!');
            }

            navCartBadge.text(cart.length);
        });
    });

    // Handling the modal confirmation message.
    $(document).on('submit', 'form[data-confirmation]', function (event) {
        var $form = $(this),
            $confirm = $('#confirmationModal');

        if ($confirm.data('result') !== 'yes') {
            //cancel submit event
            event.preventDefault();

            $confirm
                .off('click', '#btnYes')
                .on('click', '#btnYes', function () {
                    $confirm.data('result', 'yes');
                    $form.find('input[type="submit"]').attr('disabled', 'disabled');
                    $form.submit();
                })
                .modal('show');
        }
    });
})(window.jQuery);
