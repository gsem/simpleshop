<?php


namespace Tests\AppBundle\Validator;


use AppBundle\Entity\Item;
use AppBundle\Entity\ItemOrder;
use AppBundle\Entity\User;
use AppBundle\Validator\Constraints\OwnItemConstraint;
use AppBundle\Validator\Constraints\UniqueOrderConstraint;
use AppBundle\Validator\OwnItemConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class OwnItemConstraintValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function should_return_violation_if_seller_and_requester_is_the_same()
    {
        $context = $this->createMock(ExecutionContextInterface::class);
        $violationBuilder = $this->createMock(ConstraintViolationBuilderInterface::class);
        $context->expects($this->once())->method('buildViolation')->willReturn($violationBuilder);

        $itemOrder = $this->createMock(ItemOrder::class);
        $user = $this->createMock(User::class);
        $item = $this->createMock(Item::class);

        $user->expects($this->exactly(2))->method('getId')->willReturn(10);
        $item->expects($this->once())->method('getSeller')->willReturn($user);
        $itemOrder->expects($this->once())->method('getItem')->willReturn($item);
        $itemOrder->expects($this->once())->method('getUser')->willReturn($user);


        $this->getValidator($context)->validate($itemOrder, new OwnItemConstraint());
    }

    /**
     * @test
     */
    public function should_not_return_violation_if_ids_does_not_match()
    {
        $context = $this->createMock(ExecutionContextInterface::class);
        $context->expects($this->never())->method('buildViolation');

        $itemOrder = $this->createMock(ItemOrder::class);
        $user = $this->createMock(User::class);
        $otherUser = $this->createMock(User::class);
        $item = $this->createMock(Item::class);

        $user->expects($this->once())->method('getId')->willReturn(10);
        $otherUser->expects($this->once())->method('getId')->willReturn(55);
        $item->expects($this->once())->method('getSeller')->willReturn($user);
        $itemOrder->expects($this->once())->method('getItem')->willReturn($item);
        $itemOrder->expects($this->once())->method('getUser')->willReturn($otherUser);


        $this->getValidator($context)->validate($itemOrder, new OwnItemConstraint());
    }

    /**
     * @test
     */
    public function should_throw_exception_if_trying_to_validate_different_constraint()
    {
        $itemOrder = $this->createMock(ItemOrder::class);
        $context = $this->createMock(ExecutionContextInterface::class);

        $this->expectException(ValidatorException::class);

        $this->getValidator($context)->validate($itemOrder, new UniqueOrderConstraint());
    }

    private function getValidator(ExecutionContextInterface $context)
    {
        $validator = new OwnItemConstraintValidator();
        $validator->initialize($context);

        return $validator;
    }
}
