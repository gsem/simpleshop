<?php


namespace Tests\AppBundle\Services;


use AppBundle\Entity\Item;
use AppBundle\Entity\ItemOrder;
use AppBundle\Services\AmountCalculator;

class AmountCalculatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function should_return_zero_on_empty()
    {
        $this->assertEquals(0, $this->getCalculator()->calculateOrderTotal([]));
    }

    /**
     * @test
     */
    public function should_correctly_calculate_sum()
    {
        $itemOrderMock = $this->createMock(ItemOrder::class);
        $itemMock = $this->createMock(Item::class);

        $itemMock->expects($this->exactly(3))->method('getPrice')->willReturnOnConsecutiveCalls(1.55, 2.466, 3);
        $itemOrderMock->expects($this->exactly(3))->method('getItem')->willReturn($itemMock);

        $result = $this->getCalculator()->calculateOrderTotal(
            [
                $itemOrderMock,
                $itemOrderMock,
                $itemOrderMock,
            ]
        );

        $this->assertEquals(7.016, $result);
    }

    private function getCalculator()
    {
        return new AmountCalculator();
    }
}
