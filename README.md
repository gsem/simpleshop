simpleshop
==========
A Symfony project created on January 21, 2017, 7:27 pm.

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/5fbd89e3-dabf-4574-8fc3-77e458cc55ad/big.png)](https://insight.sensiolabs.com/projects/5fbd89e3-dabf-4574-8fc3-77e458cc55ad)

# How to run #
1. composer install
2. start the server bin/console s:r
3. set the permissions of var directory if needed.

There is no need to migrate anything as the sqlite database is already in the repo. (not a very good practice, but it fits very well in this case)

do not forget to enable gd extension.

## Data ##

The database is already populated.

There are two users:

1. demo/demo
2. demobuyer/demo

The first one is the seller of the majority of items, the second one is more of a buyer. There are some interactions simulated, that are reflected in the shopping cart, orders and initial homepage.

## Workflow ##

The new symfony workflow component was used for handling orders.

The bundle generates a nice state chart which can be seen below. The states of the order are in circles, the transitions are marked as squares.

![workflow.png](https://bitbucket.org/repo/Rbkybq/images/3996204034-workflow.png)

## Questions ##

If you have any - feel free to contact me!